package ru.demo.rest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ru.manufacture.jaction.model.Group;
import ru.manufacture.jaction.model.Role;
import ru.manufacture.jaction.security.IGroup;
import ru.manufacture.jaction.security.IRole;
import ru.manufacture.jaction.security.IUserCredential;
import ru.manufacture.persist.Declarable;
import ru.manufacture.persist.DeclarableDao;
import ru.manufacture.spring.Spring;


import static java.util.Arrays.asList;

/**
 * @date 03.08.2016.
 */
class User {
    public static IUserCredential currentUser() {
        return new IUserCredential(){
            @Override
            public String getLogin() {
                return "root";
            }

            @Override
            public Set<IRole> getRoles() {
                DeclarableDao declarableDao = Spring.bean("declarableDao");
                List roles = declarableDao.getDeclarable(Role.class, asList("BANK.STAFF", "GENERAL.ACCOUNTANT", "COMPANY.STAFF", "actor1", "actor2"));
                return new HashSet<>(roles);
            }

            @Override
            public Set<IGroup> getGroups() {
                return new HashSet<IGroup>(){{
                    add(Group.Any);
                }};
            }
        };
    }
}
