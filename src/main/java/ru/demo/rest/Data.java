package ru.demo.rest;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.context.request.WebRequest;

/**
 * @date 08.08.2016.
 */
public class Data {
    private WebRequest request;
    private Map<String, Object> response;

    public Data(WebRequest request) {
        this.request = request;
        this.response = new HashMap<>();
    }

    public WebRequest in() {
        return request;
    }

    public Map<String, Object> out() {
        return response;
    }
}
