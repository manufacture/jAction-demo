package ru.demo.rest;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import ru.manufacture.jaction.protocol.DeployProcess;
import ru.manufacture.spring.Spring;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @date 08.08.2016.
 */
public class AutoDeployer implements ServletContextListener {
    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        TransactionTemplate transaction = Spring.bean("transaction");
        transaction.execute(new TransactionCallback() {
            @Override
            public Object doInTransaction(TransactionStatus transactionStatus) {
                String sourcePattern = servletContextEvent.getServletContext().getInitParameter("auto-deploy");
                if (!isBlank(sourcePattern)) {
                    DeployProcess deploy = new DeployProcess(User.currentUser(), sourcePattern);
                    deploy.invoke();
                }
                return null;
            }
        });
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
