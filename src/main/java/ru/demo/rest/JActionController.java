package ru.demo.rest;

import java.util.Date;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ru.manufacture.jaction.model.view.ActionInstanceFilter;
import ru.manufacture.jaction.model.view.VActionInstance;
import ru.manufacture.jaction.protocol.CompleteAction;
import ru.manufacture.jaction.protocol.ExecuteAction;
import ru.manufacture.jaction.protocol.GetActionInstances;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.protocol.ProcessResponse;
import ru.manufacture.jaction.protocol.StartProcess;
import ru.manufacture.jaction.support.ProcessService;
import ru.manufacture.spring.Spring;
import ru.manufacture.types.Interval;
import ru.manufacture.types.format.Format;
import ru.manufacture.util.pageable.SimplePageable;

/**
 * @date 03.08.2016.
 */
@Controller
public class JActionController {
    private TransactionTemplate transaction = Spring.bean("transaction");

    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String welcome() {
        return "welcome";
    }

    @RequestMapping(value = "start/{process}", method = RequestMethod.GET)
    public ModelAndView startProcess(@PathVariable String process, WebRequest webRequest) {
        ProcessRequest<Data> request = new ProcessRequest<>(new Data(webRequest));
        final StartProcess<Data> start = new StartProcess<>(User.currentUser(), process, request);
        TransactionCallback<ProcessResponse<Data>> callback = new TransactionCallback<ProcessResponse<Data>>() {
            @Override
            public ProcessResponse<Data> doInTransaction(TransactionStatus transactionStatus) {
                return start.invoke();
            }
        };
        ProcessResponse<Data> response = transaction.execute(callback);
        if (response.isActionAvailable()) {
            return new ModelAndView("redirect:/execute/" + response.getActionId());
        } else {
            return new ModelAndView("redirect:/inbox");
        }
    }

    @RequestMapping(value = "execute/{actionId}", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView executeAction(@PathVariable long actionId, WebRequest webRequest) {
        ProcessRequest<Data> request = new ProcessRequest<>(new Data(webRequest));
        final ExecuteAction<Data> execute = new ExecuteAction<>(User.currentUser(), actionId, request);
        TransactionCallback<ProcessResponse<Data>> callback = new TransactionCallback<ProcessResponse<Data>>() {
            @Override
            public ProcessResponse<Data> doInTransaction(TransactionStatus transactionStatus) {
                return execute.invoke();
            }
        };
        ProcessResponse<Data> response = transaction.execute(callback);

        if (response.isActionAvailable()) {
            Map<String, Object> out = response.getData().out();
            out.put("actionId", response.getActionId());
            return new ModelAndView(response.getView(), out);
        } else {
            return new ModelAndView("redirect:/inbox");
        }
    }

    @RequestMapping(value = "complete/{actionId}", method = RequestMethod.POST)
    public ModelAndView completeAction(@PathVariable long actionId, WebRequest webRequest) {
        ProcessRequest<Data> request = new ProcessRequest<>(new Data(webRequest));
        final CompleteAction<Data> complete = new CompleteAction<>(User.currentUser(), actionId, request);
        TransactionCallback<ProcessResponse<Data>> callback = new TransactionCallback<ProcessResponse<Data>>() {
            @Override
            public ProcessResponse<Data> doInTransaction(TransactionStatus transactionStatus) {
                return complete.invoke();
            }
        };
        ProcessResponse<Data> response = transaction.execute(callback);
        if (response.isActionAvailable()) {
            return new ModelAndView("redirect:/execute/" + response.getActionId());
        } else {
            return new ModelAndView("redirect:/inbox");
        }
    }

    @RequestMapping(
            value = "inbox",
            method = RequestMethod.GET
    )
    public String inbox(@RequestParam(required = false) final String fromDate,
                        @RequestParam(required = false) final String toDate,
                        @RequestParam(required = false) final Long processId,
                        @RequestParam(required = false) final String description,
                        @RequestParam(required = false) final Integer pageSize,
                        @RequestParam(required = false) final Long currentPage,
                        final ModelMap model) {
        TransactionCallback<String> callback = new TransactionCallback<String>() {
            @Override
            public String doInTransaction(TransactionStatus transactionStatus) {
                Date from = Format.toObject(Date.class, fromDate);
                if (from == null) {
                    from = Format.toObject(Date.class, "01.01.2015");
                }
                Date to = Format.toObject(Date.class, toDate);
                Interval<Date> interval = new Interval<>(from, to);
                ActionInstanceFilter filter = new ActionInstanceFilter(interval, processId, description, pageSize, currentPage);
                final GetActionInstances getActionInstances = new GetActionInstances(User.currentUser(), filter);
                SimplePageable<VActionInstance> pageable = getActionInstances.invoke();
                model.addAttribute("pageable", pageable);
                model.addAttribute("filter", filter);
                model.addAttribute("dateFormat", Format.instance(Date.class));
                ProcessService processService = Spring.bean("processService");
                model.addAttribute("processList", processService.getProcesses());
                return "inbox";
            }
        };
        return transaction.execute(callback);
    }
}
