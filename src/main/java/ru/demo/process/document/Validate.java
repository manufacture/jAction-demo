package ru.demo.process.document;

import ru.demo.model.Document;
import ru.demo.rest.Data;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.ExecuteSystemAction;


import static org.apache.commons.lang.Validate.notNull;

/**
 * @date 08.08.2016.
 */
public class Validate extends DocumentAction implements ExecuteSystemAction<Data> {
    @Override
    public void executeSystemAction(ProcessRequest<Data> request) {
        try {
            Document document = getDocument();
            notNull(document.getName(), "Не заполнено название документа");
            notNull(document.getDescription(), "Не заполнено описание документа");
            notNull(document.getSum(), "Не заполнена сумма документа");
            notNull(document.getCurrency(), "Не заполнена валюта документа");
            notNull(document.getAccount(), "Не заполнен счет документа");
            notNull(document.getNumber(), "Не заполнен номер документа");
            setVar("valid", true);
        } catch (Exception e) {
            setVar("valid", false);
            setVar("message", e.getMessage());
        }
    }
}
