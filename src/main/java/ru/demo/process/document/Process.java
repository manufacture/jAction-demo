package ru.demo.process.document;

import java.util.Date;
import ru.demo.model.Document;
import ru.demo.rest.Data;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.CompleteAction;
import ru.manufacture.jaction.support.algorithm.CreateDescription;
import ru.manufacture.jaction.support.algorithm.ExecuteAction;
import ru.manufacture.spring.Hibernate;

/**
 * @date 08.08.2016.
 */
public class Process extends DocumentAction implements ExecuteAction<Data>, CompleteAction<Data>, CreateDescription {
    @Override
    public void completeAction(ProcessRequest<Data> request) {
        Document document = getDocument();
        document.setProcessed(true);
        document.setProcessDate(new Date());
        Hibernate.instance().session().saveOrUpdate(document);
    }

    @Override
    public String createDescription() {
        return "Обработка документа";
    }

    @Override
    public Data executeAction(ProcessRequest<Data> request) {
        request.getData().out().put("document", getDocument());
        request.getData().out().put("disabled", "disabled");
        return request.getData();
    }
}
