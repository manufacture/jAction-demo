package ru.demo.process.document;

import java.util.Date;
import ru.demo.model.Document;
import ru.manufacture.jaction.support.algorithm.JAction;
import ru.manufacture.spring.Hibernate;

/**
 * @date 08.08.2016.
 */
public class DocumentAction extends JAction {

    protected Document getDocument() {
        Long documentID = getVar("documentID");
        Document doc;
        if (documentID == null) {
            doc = new Document();
            doc.setRegDate(new Date());
            documentID = (Long) Hibernate.instance().session().save(doc);
            setVar("documentID", documentID);
        } else {
            doc = (Document) Hibernate.instance().session().get(Document.class, documentID);
        }
        return doc;
    }
}
