package ru.demo.process.document;

import java.math.BigDecimal;
import ru.demo.model.Document;
import ru.demo.rest.Data;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.CompleteAction;
import ru.manufacture.jaction.support.algorithm.CreateDescription;
import ru.manufacture.jaction.support.algorithm.ExecuteAction;
import ru.manufacture.spring.Hibernate;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @date 08.08.2016.
 */
public class Create extends DocumentAction implements ExecuteAction<Data>, CompleteAction<Data>, CreateDescription {
    @Override
    public Data executeAction(ProcessRequest<Data> request) {
        request.getData().out().put("document", getDocument());
        request.getData().out().put("valid", getVar("valid"));
        request.getData().out().put("message", getVar("message"));
        return request.getData();
    }

    @Override
    public  void completeAction(ProcessRequest<Data> request) {
        Document document = getDocument();
        document.setName(request.getData().in().getParameter("name"));
        document.setDescription(request.getData().in().getParameter("description"));
        document.setNumber(request.getData().in().getParameter("number"));
        String sum = request.getData().in().getParameter("sum");
        document.setSum(isBlank(sum) ? null : new BigDecimal(sum));
        document.setCurrency(request.getData().in().getParameter("currency"));
        document.setAccount(request.getData().in().getParameter("account"));
        document.setSigned(false);
        document.setProcessed(false);
        Hibernate.instance().session().saveOrUpdate(document);
    }

    @Override
    public String createDescription() {
        return "Заполнение реквизитов документа";
    }
}
