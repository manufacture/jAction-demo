package ru.demo.process.document;

import java.util.Date;
import ru.demo.model.Document;
import ru.demo.rest.Data;
import ru.manufacture.jaction.protocol.ProcessRequest;
import ru.manufacture.jaction.support.algorithm.ExecuteSystemAction;
import ru.manufacture.spring.Hibernate;

/**
 * @date 08.08.2016.
 */
public class Send extends DocumentAction implements ExecuteSystemAction<Data> {
    @Override
    public void executeSystemAction(ProcessRequest<Data> request) {
        Document document = getDocument();
        document.setSendDate(new Date());
        Hibernate.instance().session().saveOrUpdate(document);
    }
}
