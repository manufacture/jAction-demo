<%--
  Created by IntelliJ IDEA.
  User: DegRA
  Date: 03.08.2016
  Time: 17:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="ru.manufacture.jaction.model.view.VActionInstance" %>
<%@ page import="ru.manufacture.util.pageable.SimplePageable" %>
<%@ page import="ru.manufacture.jaction.model.Process" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.manufacture.jaction.model.view.ActionInstanceFilter" %>

<jsp:include page="header.jsp"/>

<script>
    function executeAction(actionId) {
        document.inboxForm.action = 'execute/' + actionId;
        document.inboxForm.submit();
    }

    function viewPage(n) {
        document.filterForm.currentPage.value = n;
        document.filterForm.submit();
    }
</script>

<ul class="f-nav f-nav-tabs">
    <li><a href="/jaction">Главная страница</a></li>
    <li><a href="start/process[demo1]">Создать тестовый документ</a></li>
    <li class="active"><a href="#">Мои задачи</a></li>
</ul>

<form name="filterForm" method="get" action="inbox">
    <input type="hidden" name="pageSize" value="15"/>
    <input type="hidden" name="currentPage" value="${filter.currentPage}">
    <div class="f-row">
        <label>Период</label>
        <div class="f-input">
            <label>
                с <input type="text" class="g-2" name="fromDate" value="${dateFormat.format(filter.dateInterval.fromValue)}"/>
                по <input type="text" class="g-2" name="toDate" value="${dateFormat.format(filter.dateInterval.toValue)}"/>
            </label>
        </div>
    </div>
    <div class="f-row">
        <label>Процесс</label>
        <div class="f-input">
            <select name="processId">
                <option value="0"></option>
                <%
                    List<Process> processList = (List<Process>) request.getAttribute("processList");
                    ActionInstanceFilter filter = (ActionInstanceFilter) request.getAttribute("filter");
                    for (Process process : processList) {%>
                <option value="<%=process.getId()%>" <%=process.getId().equals(filter.getProcessId()) ? "selected" : ""%>><%=process.getName()%></option>
                <%  }%>
            </select>
        </div>
    </div>
    <div class="f-row">
        <label>Описание</label>
        <div class="f-input">
            <input type="text" class="g-4" name="description" value="${filter.description}"/>
        </div>
    </div>
    <div class="f-actions">
        <button type="button" class="f-bu" onclick="viewPage(1);">Поиск</button>
    </div>
</form>

<%
    SimplePageable<VActionInstance> pageable = (SimplePageable<VActionInstance>) request.getAttribute("pageable");
%>
<form name="inboxForm" method="post">
    <div class="f-pager">
        <ul>
            <%if (pageable.getCurrentPage() > 1) {%>
            <li class="f-pager-prev"><a href="javascript:viewPage(1);">Начало</a></li>
            <li class="f-pager-prev"><a href="javascript:viewPage(<%=pageable.getCurrentPage() - 1%>);">← Сюда</a></li>
            <%} else {%>
            <li class="f-pager-prev"><span>Начало</span></li>
            <li class="f-pager-prev"><span>← Сюда</span></li>
            <%}%>
            <%if (pageable.getCurrentPage() > pageable.getPageDiapasonSize()) {%>
            ...
            <%}%>
            <%for (long pageNum : pageable.getPagesToDisplay()) {
                if (pageNum != pageable.getCurrentPage()) {
            %>

            <li><a href="javascript:viewPage(<%=pageNum%>);"><%=pageNum%></a></li>

            <%      } else {
            %>

            <li class="active"><strong><%=pageNum%></strong></li>

            <%      }
            }
            %>
            <%if (pageable.getCurrentPage() <= (pageable.getPageCount() - pageable.getPageDiapasonSize())) {%>
            ...
            <%}%>
            <%if (pageable.getCurrentPage() < pageable.getPageCount()) {%>
            <li class="f-pager-next"><a href="javascript:viewPage(<%=pageable.getCurrentPage() + 1%>);">Туда →</a></li>
            <li class="f-pager-next"><a href="javascript:viewPage(<%=pageable.getPageCount()%>);">Конец</a></li>
            <%} else {%>
            <li class="f-pager-next"><span>Туда →</span></li>
            <li class="f-pager-next"><span>Конец</span></li>
            <%}%>
        </ul>
    </div>
    <table class="f-table-zebra">
        <caption>
            <span>Всего: <%=pageable.getTotalRows()%> (Отображено: <%=pageable.getData().size()%>) Всего страниц: <%=pageable.getPageCount()%></span>
        </caption>
        <thead>
        <tr>
            <th>№</th>
            <th width="10%">Процесс</th>
            <th width="20%">Работа</th>
            <th width="20%">Роль</th>
            <th width="20%">Группа</th>
            <th width="30%">Описание</th>
            <th>&nbsp;</td>
        </tr>
        </thead>
        <tbody>
        <% for (VActionInstance item : pageable.getData()) {%>
        <tr>
            <td><%=item.getRowNumber()%></td>
            <td><%=item.getProcessName()%></td>
            <td><%=item.getActionName()%></td>
            <td><%=item.getRoleName()%></td>
            <td><%=item.getGroupName()%></td>
            <td><%=item.getDescription()%></td>
            <td><button type="button" class="f-bu" onclick="executeAction(<%=item.getActionInstance()%>);">Открыть</button></td>
        </tr>
        <%}%>
        </tbody>
    </table>
</form>

<jsp:include page="footer.jsp"/>