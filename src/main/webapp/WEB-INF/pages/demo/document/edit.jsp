<%--
  Created by IntelliJ IDEA.
  User: DegRA
  Date: 08.08.2016
  Time: 17:02
  To change this template use File | Settings | File Templates.
--%>
<div class="f-row">
    <label>Название</label>
    <div class="f-input">
        <input type="text" class="g-4" name="name" value="${document.name}" ${disabled}>
    </div>
</div>
<div class="f-row">
    <label>Описание</label>
    <div class="f-input">
        <input type="text" class="g-4" name="description" value="${document.description}" ${disabled}>
    </div>
</div>
<div class="f-row">
    <label>Номер</label>
    <div class="f-input">
        <input type="text" class="g-3" name="number" value="${document.number}" ${disabled}>
    </div>
</div>
<div class="f-row">
    <label>Сумма</label>
    <div class="f-input">
        <input type="text" class="g-2" name="sum" value="${document.sum}" ${disabled}>
    </div>
</div>
<div class="f-row">
    <label>Валюта</label>
    <div class="f-input">
        <input type="text" class="g-2" name="currency" value="${document.currency}" ${disabled}>
    </div>
</div>
<div class="f-row">
    <label>Номер счета</label>
    <div class="f-input">
        <input type="text" class="g-4" name="account" value="${document.account}" ${disabled}>
    </div>
</div>
