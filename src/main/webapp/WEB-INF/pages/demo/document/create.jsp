<%--
  Created by IntelliJ IDEA.
  User: DegRA
  Date: 08.08.2016
  Time: 12:53
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../../header.jsp"/>

<c:if test="${not empty message}">
    <div class="f-message f-message-error">
        <span class="close"></span>
        <h5>Ошибка заполнения данных</h5>
        <p>${message}</p>
    </div>
</c:if>

<ul class="f-nav f-nav-tabs">
    <li><a href="/jaction">Главная страница</a></li>
    <li class="active"><a href="#">Тестовый документ</a></li>
    <li><a href="/jaction/inbox">Мои задачи</a></li>
</ul>

<form method="post" action="/jaction/complete/${actionId}">
<h2>Создание документа</h2>
<jsp:include page="edit.jsp"/>

<button type="submit" class="f-bu">Сохранить</button>
<button type="button" class="f-bu" onclick="document.location = '/jaction/inbox';">Выход</button>

</form>

<jsp:include page="../../footer.jsp"/>